:Altera o IP
netsh interface ip set address "Ethernet" static address=192.168.1.64 mask=255.255.255.0 gateway=192.168.1.1

:Altera o servidor de DNS primario
netsh interface ip set dns name="Ethernet" static 208.67.222.222

:Altera o servidor de DNS secundario
netsh interface ip add dns name="Ethernet" 208.67.220.220 index=2